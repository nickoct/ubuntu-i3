#!/bin/bash
read -p "Please enter your username: " name

# Update Repositories
sudo dpkg --configure -a
sudo apt-get update -y

# Copy relevant Configuration Files
mv Scripts ~/
cp ~/Scripts/conky-i3bar ~/ && cp ~/Scripts/.conkyrci3 ~/ && cp -Rf ~/Scripts/.config ~/ && cp ~/Scripts/.bashrc ~/
cp -Rf ~/Scripts/.epsxe ~/ && cp -Rf ~/Scripts/.fonts ~/ && cp -Rf ~/Scripts/.urxvt ~/
cp -Rf ~/Scripts/.vimrc ~/ && cp -Rf ~/Scripts/.Xresources ~/ && cp -Rf ~/Scripts/.xinitrc ~/ && cp ~/Scripts/Minecraft.jar ~/

# Copy same files to etc/skel
sudo cp ~/Scripts/conky-i3bar /etc/skel/ && sudo cp ~/Scripts/.conkyrci3 /etc/skel/ && sudo cp -Rf ~/Scripts/.config /etc/skel/
sudo cp -Rf ~/Scripts/.epsxe /etc/skel/ && sudo cp -Rf ~/Scripts/.fonts /etc/skel/ && sudo cp -Rf ~/Scripts/.urxvt /etc/skel/
sudo cp -Rf ~/Scripts/.vimrc /etc/skel/ && sudo cp -Rf ~/Scripts/.Xresources /etc/skel/ && sudo cp -Rf ~/Scripts/.xinitrc /etc/skel/
sudo cp -Rf ~/Scripts/.bashrc /etc/skel/ && sudo cp ~Scripts/Minecraft.jar /etc/skel/
sudo cp -Rf ~/Scripts/.screenlayout/  /etc/skel/ && cp -Rf ~/Scripts/.screenlayout/ ~/

# Installing Virtualisation
sudo apt install virtualbox-guest-utils virtualbox-guest-dkms virtualbox-guest-additions-iso open-vm-tools open-vm-tools-desktop dkms build-essential linux-headers-$(uname -r) -y

# Installing Virt-Manager
 sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin virt-manager -y 
sudo adduser $(id -un) libvirt
sudo adduser $(id -un) libvirt-qemu
sudo adduser $(id -un) kvm

# Installing latest kodi
sudo apt install software-properties-common -y
sudo add-apt-repository -y ppa:team-xbmc/ppa
sudo apt install kodi -y

sudo apt-get install libsdl2-dev libboost-system-dev libboost-filesystem-dev libboost-date-time-dev libboost-locale-dev libfreeimage-dev libfreetype6-dev libeigen3-dev libcurl4-openssl-dev libasound2-dev libgl1-mesa-dev build-essential cmake fonts-droid-fallback -y
# Compiling EmulationStation
git clone https://github.com/Aloshi/EmulationStation.git
cd EmulationStation
cmake .
make
sudo cp emulationstation /usr/bin/
cd ..
sudo rm -r EmulationStation

# Compiling Libretro Emulators
## PCSX_ReArmed - Playstation  Emulator
git clone https://github.com/libretro/pcsx_rearmed.git
cd pcsx_rearmed
make -f Makefile.libretro
sudo cp pcsx_rearmed_libretro.so /usr/lib/x86_64-linux-gnu/libretro/
cd .. && rm -r pcsx_rearmed
## fbalpha2012_neogeo - Final Burn Alpha Neogeo
git clone https://github.com/libretro/fbalpha2012_neogeo.git
cd fbalpha2012_neogeo/
make
sudo cp fbalpha2012_neogeo_libretro.so /usr/lib/x86_64-linux-gnu/libretro/
cd .. && sudo rm -r fbalpha2012_neogeo

# Installing Java 8 for Minecraft
wget -O ~/Minecraft.deb https://launcher.mojang.com/download/Minecraft.deb 
sudo gdebi ~/Minecraft.deb -y
sudo apt-get install openjdk-8-jdk openjdk-8-jdk-headless openjdk-8-jre openjdk-8-jre-headless openjdk-8-source default-jre default-jre-headless openjdk-11-jre openjdk-11-jre-headless -y
sudo rm ~/Minecraft.deb
# Install i3-System and programs
sudo apt-get update -y
sudo apt-get install i3status i3blocks playerctl rofi neofetch libghc-tidal-prof wildmidi timidity obs-studio handbrake handbrake-cli nitrogen compton gdebi galternatives lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings  rxvt-unicode vim vim-syntax-gtk steam aisleriot asunder aac-enc fdkaac tmux ranger ncmpcpp mpd libmpdclient2 mate-backgrounds mpv -y
sudo rm /etc/mpd.conf
sudo apt-get install w3m w3m-img -y
# Install Xbox360 drivers
sudo apt-get install xboxdrv -y
sudo apt remove -y xserver-xorg-input-joystick
sudo echo "blacklist xpad" >> /etc/modprobe/blacklist.conf
# installing cubic
sudo apt-add-repository -y ppa:cubic-wizard/release 
sudo apt-key -y adv --keyserver keyserver.ubuntu.com --recv-keys 6494C6D6997C215E 
sudo apt update -y
sudo apt install cubic -y

# install i3-gaps dependencies 
#sudo apt-get install git gcc make dh-autoreconf libxcb-keysyms1-dev #libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev -y
#sudo apt-get install libxcb-icccm4-dev libyajl-dev libev-dev #libxcb-xkb-dev libxcb-cursor-dev  -y
#sudo apt-get install libxkbcommon-dev libxcb-xinerama0-dev #libxkbcommon-x11-dev libstartup-notification0-dev -y
#sudo apt-get install libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev #libxcb-shape0-dev  -y

# clone the repository
#cd ~/
#git clone https://www.github.com/Airblader/i3 i3-gaps
#cd i3-gaps

# compile & install
#autoreconf --force --install
#rm -rf build/
#mkdir -p build && cd build/

# Disabling sanitizers is important for release versions!
# The prefix and sysconfdir are, obviously, dependent on the distribution.
#../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
#make
#sudo make install
sudo add-apt-repository -y ppa:regolith-linux/unstable 
sudo apt update -y
sudo apt install i3-gaps -y


# install polybar dependencies 
sudo apt-get install clang cmake cmake-data git pkg-config python3 python3-sphinx libcairo2-dev -y 
sudo apt-get install libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev libuv1-dev -y
sudo apt-get install xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev python3-packaging -y
sudo apt-get install python3-xcbgen libxcb-xkb-dev libxcb-xrm-dev libxcb-cursor-dev libasound2-dev libpulse-dev -y 
sudo apt-get install libjsoncpp-dev libmpdclient-dev libcurl4-openssl-dev libnl-genl-3-dev ttf-unifont ccache -y

# Comapt install build-essential git cmake cmake-data pkg-config python3-sphinx python3-packaging libuv1-dev libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-devpile Polybar
cd ~/
# Make sure to type the `git' command as is to clone all git submodules too
git clone --recursive https://github.com/polybar/polybar
cd polybar
mkdir build
cd build
cmake ..
make -j$(nproc)
# Optional. This will install the polybar executable in /usr/local/bin
sudo make install

# Remove libreoffice
sudo apt remove -y libnumbertext-1.0-0 libnumbertext-data libreoffice-avmedia-backend-gstreamer libreoffice-base libreoffice-base-core 
sudo apt remove -y libreoffice-base-drivers libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk3 
sudo apt remove -y libreoffice-help-common libreoffice-help-en-us libreoffice-impress libreoffice-java-common libreoffice-math libreoffice-report-builder-bin 
sudo apt remove -y libreoffice-sdbc-hsqldb libreoffice-style-colibre libreoffice-style-tango libreoffice-writer lo-main-helper mugshot mythes-en-us 
sudo apt remove -y printer-driver-cups-pdf uno-libs3 ure
#sudo reboot
#sudo apt-get update && sudo apt-get upgrade

# Add Polybar Configs
sudo rm -Rf ~/i3-gaps
sudo rm -Rf ~/polybar 
sudo chmod +x ~/.config/polybar/launch.sh
sudo apt-get update -y
# Compile Radiotray-ng
cd ~/Ubuntu-i3
bash Radiotray.sh
# Add IceSSB
wget https://launchpadlibrarian.net/353315608/ice_5.3.0_all.deb
sudo apt -y install gstreamer1.0-plugins-ugly
sudo dpkg -i ice_5.3.0_all.deb
sudo apt install -f 
sudo rm ice_5.3.0_all.deb
# Install Latest AMD Graphics Drivers
sudo add-apt-repository -y ppa:kisak/kisak-mesa 
sudo apt update -y && sudo apt upgrade -y
sudo apt install libgl1-mesa-dri:i386 -y
sudo apt install mesa-vulkan-drivers mesa-vulkan-drivers:i386 -y
sudo mkdir /etc/X11/xorg.conf.d
sudo cp ~/Scripts/10-monitors.conf /etc/X11/xorg.conf.d/

#Remove Unwanted Directories
sudo mv ~/Scripts/lightdm.conf /etc/lightdm
sudo sed -i "s/autologin-user=peter/autologin-user=$name/g" /etc/lightdm/lightdm.conf
sudo rm -Rf ~/Scripts 
sudo rm -rf ~/Ubuntu-i3

