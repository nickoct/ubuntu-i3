#!/bin/bash

# Compile Radiotray-ng
apt install lsb-release libcurl4-openssl-dev libjsoncpp-dev libxdg-basedir-dev libnotify-dev libboost-filesystem-dev libgstreamer1.0-dev libappindicator3-dev libboost-log-dev libboost-program-options-dev libgtk-3-dev libnotify-dev lsb-release libbsd-dev libncurses5-dev libglibmm-2.4-dev libwxgtk3.0-gtk3-dev libwxgtk3.0-gtk3-0v5 cmake gstreamer1.0-plugins-good gstreamer1.0-plugins-bad python3-lxml -y
git clone https://github.com/ebruck/radiotray-ng.git
cd radiotray-ng
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make package
dpkg -i radiotray-ng*.deb 
apt-get install -f
cd ../.. && rm -r radiotray-ng

