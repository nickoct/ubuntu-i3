#!/bin/bash

# Update Repositories
apt-get update -y

mv ../Scripts ~/
cp -Rf ~/Scripts/.emulationstation ~/ && cp ~/Scripts/conky-i3bar ~/ && cp ~/Scripts/.conkyrci3 ~/ && cp -Rf ~/Scripts/.config ~/ && cp ~/Scripts/.bashrc ~/ && cp -Rf ~/Scripts/.fonts ~/ && cp -Rf ~/Scripts/.urxvt ~/
cp -Rf ~/Scripts/.vimrc ~/ && cp -Rf ~/Scripts/.Xresources ~/ && cp -Rf ~/Scripts/.xinitrc ~/ 

# Copy same files to etc/skel
cp -Rf ~/Scripts/.emulationstation /etc/skel && cp ~/Scripts/conky-i3bar /etc/skel/ && cp ~/Scripts/.conkyrci3 /etc/skel/ && cp -Rf ~/Scripts/.config /etc/skel/ && cp -Rf ~/Scripts/.fonts /etc/skel/ && cp -Rf ~/Scripts/.urxvt /etc/skel/
cp -Rf ~/Scripts/.vimrc /etc/skel/ && cp -Rf ~/Scripts/.Xresources /etc/skel/ && cp -Rf ~/Scripts/.xinitrc /etc/skel/
cp -Rf ~/Scripts/.bashrc /etc/skel/
# Installing Virtualisation
apt install virtualbox-guest-utils virtualbox-guest-dkms virtualbox-guest-additions-iso open-vm-tools open-vm-tools-desktop dkms build-essential linux-headers-$(uname -r) -y

# Installing Virt-Manager
apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin virt-manager -y 

#Installing TPM2.0 for Windows 11 on Virt-Manager
apt-get -y install automake autoconf libtool gcc build-essential libssl-dev libtasn1-6-dev libtpms-dev net-tools iproute2 libjson-glib-dev libseccomp-dev make libgnutls28-dev expect gawk socat dh-exec pkg-config dh-autoreconf
git clone https://github.com/stefanberger/libtpms
git clone https://github.com/stefanberger/swtpm
cd libtpms
./autogen.sh --with-openssl
make dist
dpkg-buildpackage -us -uc -j12
cd ../
dpkg -i libtpms0_0.10.0-dev1_amd64.deb libtpms-dev_0.10.0-dev1_amd64.deb
cd swtpm
./autogen.sh --with-openssl --prefix=/usr
make -j12
make Install

# Installing latest kodi
apt install software-properties-common -y
add-apt-repository -y ppa:team-xbmc/ppa
apt install kodi -y

apt-get install retroarch retroarch-assets libsdl2-dev libboost-system-dev libboost-filesystem-dev libboost-date-time-dev libboost-locale-dev libfreeimage-dev libfreetype6-dev libeigen3-dev libcurl4-openssl-dev libasound2-dev libgl1-mesa-dev build-essential cmake fonts-droid-fallback -y

# Compiling EmulationStation
git clone https://github.com/Aloshi/EmulationStation.git
cd EmulationStation
cmake .
make
make install
#cp emulationstation /usr/bin/
cd ..
rm -r EmulationStation
# Add Libretro Core Emulators
cd ~/Ubuntu-i3/CubicScripts/
chmod +x libretro.sh
./libretro.sh

# Installing Java 8 for Minecraft
apt-get install openjdk-8-jdk openjdk-8-jdk-headless openjdk-8-jre openjdk-8-jre-headless openjdk-8-source default-jre default-jre-headless openjdk-11-jre openjdk-11-jre-headless -y
wget -O ~/Minecraft.deb https://launcher.mojang.com/download/Minecraft.deb 
gdebi ~/Minecraft.deb -n
rm ~/Minecraft.deb
# Install i3-System and programs
apt-get update -y
apt-get install i3status i3blocks playerctl rofi neofetch libghc-tidal-prof wildmidi timidity obs-studio handbrake handbrake-cli nitrogen compton gdebi galternatives lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings  rxvt-unicode vim vim-syntax-gtk steam aisleriot asunder aac-enc fdkaac tmux ranger ncmpcpp mpd libmpdclient2 mate-backgrounds mpv w3m w3m-img polybar -y
rm /etc/mpd.conf

# Install Xbox360 drivers
apt-get install xboxdrv -y
apt remove -y xserver-xorg-input-joystick
echo "blacklist xpad" >> /etc/modprobe/blacklist.conf

# installing cubic
apt-add-repository -y ppa:cubic-wizard/release 
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6494C6D6997C215E -y
apt update -y
apt install cubic -y

add-apt-repository -y ppa:regolith-linux/unstable 
apt update -y
apt install i3-gaps -y

# Add Polybar Configs
rm -Rf ~/i3-gaps
rm -Rf ~/polybar 
chmod +x ~/.config/polybar/launch.sh
chmod +x /etc/skel/.config/polybar/launch.sh
apt-get update -y

# Compile Radiotray-ng
cd ~/Ubuntu-i3/CubicScripts
chmod +x Cubic-Radiotray.sh
./Cubic-Radiotray.sh
cp lightdm.conf /etc/lightdm/

# Add IceSSB
wget https://launchpadlibrarian.net/353315608/ice_5.3.0_all.deb
apt install gstreamer1.0-plugins-ugly -y
dpkg -i ice_5.3.0_all.deb
apt install -f 
rm ice_5.3.0_all.deb

# Install Latest AMD Graphics Drivers
add-apt-repository -y ppa:kisak/kisak-mesa 
dpkg --add-architecture i386
apt install libgl1-mesa-dri:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386
dpkg --add-architecture i386 && apt update && apt install -y wine64 wine32 libasound2-plugins:i386 libsdl2-2.0-0:i386 libdbus-1-3:i386 libsqlite3-0:i386
add-apt-repository ppa:lutris-team/lutris
apt update -y && apt upgrade -y
apt install lutris meson libsystemd-dev pkg-config ninja-build git libdbus-1-dev libinih-dev build-essential
apt install libgl1-mesa-dri:i386 -y
apt install mesa-vulkan-drivers mesa-vulkan-drivers:i386 -y
mkdir /etc/X11/xorg.conf.d
cp ~/Scripts/10-monitors.conf /etc/X11/xorg.conf.d/

#Remove Unwanted Directories
rm -Rf ~/Scripts 
rm -rf ~/Ubuntu-i3
