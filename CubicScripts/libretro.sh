#!/bin/bash

# Store Emulator names

rm /usr/lib/x86_64-linux-gnu/libretro/*.so

for i in dolphin dosbox_core fbalpha2012_neogeo fmsx fuse gambatte genesis_plus_gx handy mame mednafen_ngp mednafen_pce_fast mednafen_pcfx mednafen_psx mednafen_vb mednafen_wswan mgba melonds mesen mupen64plus_next o2em opera picodrive ppsspp prosystem puae sameboy snes9x stella vecx virtualjaguar yabause
 
do
	wget "http://buildbot.libretro.com/nightly/linux/x86_64/latest/""$i""_""libretro.so.zip"
	unzip "$i""_""libretro.so.zip"
	mv "$i""_""libretro.so" /usr/lib/x86_64-linux-gnu/libretro/
	rm "$i""_""libretro.so.zip"
done
